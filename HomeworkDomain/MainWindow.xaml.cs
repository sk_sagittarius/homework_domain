﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HomeworkDomain
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        System.AppDomain currentDomain = AppDomain.CurrentDomain;
        public MainWindow()
        {
            InitializeComponent();
            
        }

        private void MathButtonClick(object sender, RoutedEventArgs e)
        {
            var mathDomain = AppDomain.CreateDomain("Math Domain");
            mathDomain.ExecuteAssembly(mathDomain.BaseDirectory + "MathDomain.exe");
        }

        private void DonwloadButton(object sender, RoutedEventArgs e)
        {
            var downloadDomain = AppDomain.CreateDomain("Download Domain");
            downloadDomain.ExecuteAssembly(downloadDomain.BaseDirectory + "DownloadDomain.exe");
        }
    }
}
